//
//  ViewController.m
//  ImageMetadata
//
//  Created by Spire Jankulovski on 12/30/14.
//  Copyright (c) 2014 Spire Jankulovski. All rights reserved.
//

#import "ViewController.h"
#import <ImageIO/ImageIO.h>
#import <AssetsLibrary/AssetsLibrary.h>
#import <CoreLocation/CoreLocation.h>

@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    [self writeExif];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(id)writeExif{//(e.media, 30.0, 80.0){
    
    UIImage *image = [UIImage imageNamed:@"KS_nav_ui.png"];
  // 	ENSURE_SINGLE_ARG(args,NSDictionary);
    
//    id blob = [args valueForKey:@"image"];
//    ENSURE_TYPE(blob, TiBlob);
//    UIImage* image = [blob image];
    NSString *response;
    
    if (image == nil){
        NSLog(@"image wasn't created");
        response = @"image wasn't created";
        
    } else {
        NSLog(@"image was created successfully");
        response = @"image was created successfully";
        
    }
    
    [self saveImageAndAddMetadata:image];
    
    
    /*  NSData* pngData =  UIImagePNGRepresentation(image);
     
     CGImageSourceRef source = CGImageSourceCreateWithData((CFDataRef)pngData, NULL);
     NSDictionary *metadata = (NSDictionary *) CGImageSourceCopyPropertiesAtIndex(source, 0, NULL);
     
     float latitude = [[args valueForKey:@"latitude"] floatValue];
     
     float longitude = [[args valueForKey:@"longitude"] floatValue];
     [self saveImage:image withInfo:metadata];
     
     __block UIImage *img;
     ALAssetsLibrary *assetsLibrary = [[ALAssetsLibrary alloc] init];
     [assetsLibrary enumerateGroupsWithTypes:ALAssetsGroupSavedPhotos
     usingBlock:^(ALAssetsGroup *group, BOOL *stop) {
     if (nil != group) {
     // be sure to filter the group so you only get photos
     [group setAssetsFilter:[ALAssetsFilter allPhotos]];
     
     
     [group enumerateAssetsAtIndexes:[NSIndexSet indexSetWithIndex:group.numberOfAssets - 1] options:0 usingBlock:^(ALAsset *result, NSUInteger index, BOOL *stop) {
     if (nil != result) {
     ALAssetRepresentation *repr = [result defaultRepresentation];
     // this is the most recent saved photo
     img = [UIImage imageWithCGImage:[repr fullResolutionImage]];
     
     // we only need the first (most recent) photo -- stop the enumeration
     *stop = YES;
     }
     }];
     }
     
     *stop = NO;
     } failureBlock:^(NSError *error) {
     NSLog(@"error: %@", error);
     }];*/
    
    
    
    return image;
   // return [[[TiBlob alloc] initWithImage:image] autorelease];
    
    
    
    
}
- (void)saveImageAndAddMetadata:(UIImage *)image
{
    
    NSLog(@"TEST");
    double altitudeSF = 15.0;
    double accuracyHorizontal = 1.0;
    double accuracyVertical = 1.0;
    CLLocationCoordinate2D coordSF = CLLocationCoordinate2DMake(37.732711,-122.45224);
    NSDate * nowDate = [NSDate date];
    
    
    // Format the current date and time
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"yyyy:MM:dd HH:mm:ss"];
    NSString *now = [formatter stringFromDate:[NSDate date]];
    
    
    /*NSMutableDictionary * imageMetadata = [NSMutableDictionary dictionary];
     CLLocation * loc = [[CLLocation alloc] initWithCoordinate:coordSF altitude:altitudeSF horizontalAccuracy:accuracyHorizontal verticalAccuracy:accuracyVertical timestamp:nowDate];
     // this is in case we try to acquire actual location instead of faking it with the code right above
     if ( loc ) {
     [imageMetadata setObject:[self gpsDictionaryForLocation:loc] forKey:(NSString*)kCGImagePropertyGPSDictionary];
     }*/
    
    // Exif metadata dictionary
    // Includes date and time as well as image dimensions
    NSMutableDictionary *exifDictionary = [NSMutableDictionary dictionary];
    [exifDictionary setValue:now forKey:(NSString *)kCGImagePropertyExifDateTimeOriginal];
    [exifDictionary setValue:now forKey:(NSString *)kCGImagePropertyExifDateTimeDigitized];
    [exifDictionary setValue:[NSNumber numberWithFloat:image.size.width] forKey:(NSString *)kCGImagePropertyExifPixelXDimension];
    [exifDictionary setValue:[NSNumber numberWithFloat:image.size.height] forKey:(NSString *)kCGImagePropertyExifPixelYDimension];
    NSLog(@"exifDictionary %@",exifDictionary);
    /*
     // Tiff metadata dictionary
     // Includes information about the application used to create the image
     // "Make" is the name of the app, "Model" is the version of the app
     NSMutableDictionary *tiffDictionary = [NSMutableDictionary dictionary];
     [tiffDictionary setValue:now forKey:(NSString *)kCGImagePropertyTIFFDateTime];
     [tiffDictionary setValue:@"Interlacer" forKey:(NSString *)kCGImagePropertyTIFFMake];
     
     NSString *version = [[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleShortVersionString"];
     NSString *build = [[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleVersion"];
     [tiffDictionary setValue:[NSString stringWithFormat:@"%@ (%@)", version, build] forKey:(NSString *)kCGImagePropertyTIFFModel];
     
     // Image metadata dictionary
     // Includes image dimensions, as well as the EXIF and TIFF metadata
     NSMutableDictionary *dict = [NSMutableDictionary dictionary];
     [dict setValue:[NSNumber numberWithFloat:image.size.width] forKey:(NSString *)kCGImagePropertyPixelWidth];
     [dict setValue:[NSNumber numberWithFloat:image.size.height] forKey:(NSString *)kCGImagePropertyPixelHeight];
     [dict setValue:exifDictionary forKey:(NSString *)kCGImagePropertyExifDictionary];
     [dict setValue:tiffDictionary forKey:(NSString *)kCGImagePropertyTIFFDictionary];
     
     CLLocation * loc = [[CLLocation alloc] initWithCoordinate:coordSF altitude:altitudeSF horizontalAccuracy:accuracyHorizontal verticalAccuracy:accuracyVertical timestamp:nowDate];
     // this is in case we try to acquire actual location instead of faking it with the code right above
     if ( loc ) {
     [dict setObject:[self gpsDictionaryForLocation:loc] forKey:(NSString*)kCGImagePropertyGPSDictionary];
     }
     
     ALAssetsLibrary *al = [[ALAssetsLibrary alloc] init];
     
     [al writeImageToSavedPhotosAlbum:[image CGImage]
     metadata:dict
     completionBlock:^(NSURL *assetURL, NSError *error) {
     if (error == nil) {
     // notify user image was saved
     } else {
     // handle error
     }
     }];
     
     [al release];*/
}




/*- (void) saveImage:(UIImage *)imageToSave withInfo:(NSDictionary *)info
 {
 // Get the image metadata (EXIF & TIFF)
 NSMutableDictionary * imageMetadata = [[info objectForKey:UIImagePickerControllerMediaMetadata] mutableCopy];
 // add (fake) GPS data
 CLLocationCoordinate2D coordSF = CLLocationCoordinate2DMake(37.732711,-122.45224);
 // arbitrary altitude and accuracy
 double altitudeSF = 15.0;
 double accuracyHorizontal = 1.0;
 double accuracyVertical = 1.0;
 NSDate * nowDate = [NSDate date];
 // create CLLocation for image
 CLLocation * loc = [[CLLocation alloc] initWithCoordinate:coordSF altitude:altitudeSF horizontalAccuracy:accuracyHorizontal verticalAccuracy:accuracyVertical timestamp:nowDate];
 // this is in case we try to acquire actual location instead of faking it with the code right above
 if ( loc ) {
 [imageMetadata setObject:[self gpsDictionaryForLocation:loc] forKey:(NSString*)kCGImagePropertyGPSDictionary];
 }
 // Get the assets library
 ALAssetsLibrary *library = [[ALAssetsLibrary alloc] init];
 // create a completion block for when we process the image
 ALAssetsLibraryWriteImageCompletionBlock imageWriteCompletionBlock =
 ^(NSURL *newURL, NSError *error) {
 if (error) {
 NSLog( @"Error writing image with metadata to Photo Library: %@", error );
 } else {
 NSLog( @"Wrote image %@ with metadata %@ to Photo Library",newURL,imageMetadata);
 }
 };
 // Save the new image to the Camera Roll, using the completion block defined just above
 [library writeImageToSavedPhotosAlbum:[imageToSave CGImage]
 metadata:imageMetadata
 completionBlock:imageWriteCompletionBlock];
 }
 */

/*- (NSDictionary *) gpsDictionaryForLocation:(CLLocation *)location
 {
 CLLocationDegrees exifLatitude = location.coordinate.latitude;
 CLLocationDegrees exifLongitude = location.coordinate.longitude;
 NSString * latRef;
 NSString * longRef;
 if (exifLatitude < 0.0) {
 exifLatitude = exifLatitude * -1.0f;
 latRef = @"S";
 } else {
 latRef = @"N";
 }
 if (exifLongitude < 0.0) {
 exifLongitude = exifLongitude * -1.0f;
 longRef = @"W";
 } else {
 longRef = @"E";
 }
 NSMutableDictionary *locDict = [[NSMutableDictionary alloc] init];
 // requires ImageIO
 [locDict setObject:location.timestamp forKey:(NSString*)kCGImagePropertyGPSTimeStamp];
 [locDict setObject:latRef forKey:(NSString*)kCGImagePropertyGPSLatitudeRef];
 [locDict setObject:[NSNumber numberWithFloat:exifLatitude] forKey:(NSString *)kCGImagePropertyGPSLatitude];
 [locDict setObject:longRef forKey:(NSString*)kCGImagePropertyGPSLongitudeRef];
 [locDict setObject:[NSNumber numberWithFloat:exifLongitude] forKey:(NSString *)kCGImagePropertyGPSLongitude];
 [locDict setObject:[NSNumber numberWithFloat:location.horizontalAccuracy] forKey:(NSString*)kCGImagePropertyGPSDOP];
 [locDict setObject:[NSNumber numberWithFloat:location.altitude] forKey:(NSString*)kCGImagePropertyGPSAltitude];
 return locDict;
 }*/
@end
